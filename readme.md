# Brainy Web UI

## How to configure the app for development

In the `aurelia.env` file change the values:
```
BASE_SERVER_URL=https://brainy-backend-mock.herokuapp.com
URL_REDIRECT=http://localhost:3000 (Linux) or http://192.168.99.100:3000 (Mac or Windows)
OUTLOOK_BASE_URL=https://brainy-outlook-mock.herokuapp.com
```

## How to run with docker

1. Open a bash terminal with a docker machine running.
> That is:
> 1. In Linux just open the terminal.
> 1. In Mac and Windows open the "Docker Quick Start Terminal".

1. Create the docker image with the `Dockerfile` provided on this project (execute it on the project root folder).
  ```console
  . docker-create-local-image.sh
  ```

1. Then in the same folder create your docker container based on the image created on the previous step.
  ```console
  . docker-start-local-container.sh
  ```
  Voilà! You are now inside the created container.

1. Inside the created container, bootstrap the app.
  ```console
  . bootstrap.sh
  ```
  > This step is needed just once.

1. Initialize the application and start developing by starting the watcher with Gulp.
  ```console
  gulp watch
  ```

1. Open http://localhost:3000 (Linux) or http://192.168.99.100:3000 (Mac or Windows) in your favorite browser.
