import { inject } from 'aurelia-framework';
import { Client } from './client';

@inject(Client)
export class SkillClient {

  constructor(client) {
    this.client = client;
  }

  retrieveSkills(query) {
    return this.client.getFrom(`/skillApi/skill/${query}`)
         .then(response => response.json())
         .then(jsonResponse => jsonResponse);
  }
}
