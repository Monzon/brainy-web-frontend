export default class Headquarter {
  constructor(attributes = {}) {
    this.code = attributes.code || '';
    this.name = attributes.name || '';
  }
}
