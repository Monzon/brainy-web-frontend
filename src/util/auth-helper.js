import { constants } from './constants';

export class AuthHelper {

  login() {
    const outlookUrl = `${constants.outlookBaseUrl}/authorize?response_type=token&redirect_uri=${constants.urlRedirect}&client_id=${constants.clientIdOutLook}&scope=${constants.scopeGraphOutlook}`;
    // eslint-disable-next-line no-undef
    window.location.href = outlookUrl;
  }

  parseQueryString(url) {
    const params = {};
    const regex = /([^&=]+)=([^&]*)/g;
    let match;

    // TODO refactor this smell
    // eslint-disable-next-line no-cond-assign
    while (match = regex.exec(url)) {
      params[decodeURIComponent(match[1])] = decodeURIComponent(match[2]);
    }

    return params;
  }

  validateJWT(token) {
    return this.isCorrectToken(token) && this.tokenBelongToAvantica(token);
  }

  isCorrectToken(token) {
    const base64test = /[^A-Za-z0-9+/=]/g;
    return base64test.exec(token);
  }

  tokenBelongToAvantica(token) {
    // eslint-disable-next-line no-undef
    const jsonWebToken = jwt_decode(token);
    const email = jsonWebToken.unique_name;
    return /^"?[\w-_.]*"?@avantica\.net$/.test(email);
  }

}
