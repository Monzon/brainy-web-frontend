/* global jwt_decode */

import { inject, LogManager } from 'aurelia-framework';
import { BaseComponent } from '../../util/base-component';
import { User } from '../../models/user';
import { constants } from '../../util/constants';
import { ProfileClient } from '../../clients/profile-client';
import localStorageManager from '../../util/local-storage-manager';
import { constants as constantsProfile } from '../../util/constant-profile-messages';

@inject(BaseComponent, ProfileClient)
export class ProfileService {

  constructor(baseComponent, profileClient) {
    this.baseComponent = baseComponent;
    this.profileClient = profileClient;
    this.logger = LogManager.getLogger(ProfileService.name);
  }

  setViewModel(value) {
    this.viewModel = value;
  }

  retrieveProfileInformation(username) {
    const firstLogin = localStorageManager.isFirstLogin();
    const userJwt = this.obtainUserFromJwt();
    return this.profileClient.getProfileInformation(username)
      .then((user) => {
        if (firstLogin) {
          user.setNames(userJwt.names);
        }
        const isCurrentUser = localStorageManager.isCurrentUser(user.email);
        user.setCurrentUser(isCurrentUser);
        this.viewModel.setUser(user);
      })
      .catch((error) => {
        this.logger.error('Details of the error', error);
        this.baseComponent.showMessageError();
      });
  }

  getProfileMinimalInformation(username) {
    return this.profileClient.getProfileMinimalInformation(username)
      .then(users => users[0])
      .catch((error) => { this.logger.error('Details of the error', error); });
  }

  getDashboardInformation(email) {
    return this.profileClient.getDashboardInformation(email)
      .catch((error) => { this.logger.error('Details of the error', error); });
  }

  loadInitialProfileInformation() {
    const user = this.obtainUserFromJwt();
    return this.profileClient.requestRawProfileInformation(user.email);
  }

  saveUserInformation(user) {
    this.baseComponent.showProgressHub();

    return this.profileClient.saveUserInformation(user)
      .then(userSavedResponse => this.processProfileSavedResponse(userSavedResponse))
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.baseComponent.showMessageError();
        return false;
      })
      .then((response) => {
        this.baseComponent.dismissProgressHub();
        return response;
      });
  }

  processProfileSavedResponse(response) {
    if ('messageCode' in response) {
      const message = constantsProfile.PROFILE_MESSAGECODE[response.messageCode];
      this.baseComponent.showMessageError(message);
      return false;
    }

    const user = new User(response);
    this.updateWidgetWhenUserLogged();
    this.baseComponent.showMessageSuccess('Your profile was successfully updated!');
    this.viewModel.setUser(user);
    return true;
  }

  updateWidgetWhenUserLogged() {
    localStorageManager.setFirstLogin(false);
    this.notifyUpdateNavBarWhenUserLogged();
  }

  notifyUpdateNavBarWhenUserLogged() {
    this.baseComponent.notify(constants.UPDATE_NAV_BAR_WHEN_USER_LOGGED);
  }

  showMessageError(message) {
    this.baseComponent.showMessageError(message);
  }

  obtainUserFromJwt() {
    const jsonBody = localStorageManager.getJsonWebTokenBody();

    return new User({
      names: jsonBody.name,
      email: jsonBody.unique_name,
    });
  }
}
