import { inject, LogManager } from 'aurelia-framework';
import { BaseComponent } from '../../util/base-component';
import { AcknowledgmentClient } from '../../clients/acknowledgment-client';

@inject(BaseComponent, AcknowledgmentClient)
export class AcknowledgmentService {

  constructor(baseComponent, acknowledgmentClient) {
    this.baseComponent = baseComponent;
    this.logger = LogManager.getLogger(AcknowledgmentService.name);
    this.acknowledgmentClient = acknowledgmentClient;
  }

  setViewModel(value) {
    this.viewModel = value;
  }

  retrieveAwards(useremail, headquarter) {
    this.acknowledgmentClient.getAcknowledgments(useremail, headquarter)
      .then((response) => {
        this.viewModel.setAwards(response);
        return response;
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.baseComponent.showMessageError();
        return error;
      })
      .then(response => response);
  }

}
