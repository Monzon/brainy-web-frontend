import { bindable } from 'aurelia-framework';

export class Search {
  @bindable execute;
  safeQuery = '';

  executeSearch() {
    this.execute({ query: this.query });
  }

  get query() {
    return this.safeQuery;
  }

  set query(newValue) {
    this.safeQuery = newValue;
  }
}
