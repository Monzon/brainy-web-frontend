import { Backend } from 'aurelia-i18n';

const namespaces = [
  'translation',
  'landing',
  'home',
  'profile',
  'tags',
  'acknowledgments',
];

const translationConfig = (aurelia) => {
  const setup = (instance) => {
    instance.i18next.use(Backend.with(aurelia.loader));

    return instance.setup({
      backend: {
        loadPath: 'locales/{{lng}}/{{ns}}.json',
      },
      lng: 'en',
      attributes: ['translate'],
      ns: namespaces,
      defaultNS: 'translation',
      fallbackLng: 'en',
      debug: false,
    });
  };

  return setup;
};

export default translationConfig;
