export default {
  groupNominations: (nominations) => {
    const notAcknowledged = nominations.filter(n => !n.ackId);
    const ackIds = nominations.reduce((arr, n) => {
      if (!n.ackId || arr.indexOf(n.ackId) !== -1) { return arr; }
      return arr.concat([n.ackId]);
    }, []);
    const grouped = ackIds.reduce((a, b) => a
    .concat([nominations.filter(n => n.ackId === b)[0]]), []);

    return notAcknowledged.concat(grouped);
  },
  getNominationsByAckId: (nominations, ackId) => nominations.filter(n => n.ackId === ackId),
};
