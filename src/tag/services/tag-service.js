import { inject, LogManager } from 'aurelia-framework';
import { BaseComponent } from '../../util/base-component';
import { TagClient } from '../../clients/tag-client';

@inject(BaseComponent, TagClient)
export class TagService {

  constructor(baseComponent, tagClient) {
    this.baseComponent = baseComponent;
    this.tagClient = tagClient;
    this.logger = LogManager.getLogger(TagService.name);
  }

  getTagListByUser() {
    this.baseComponent.showProgressHub();

    return this.tagClient
      .getTagListByUser()
      .then((tags) => {
        this.viewModel.setTags(tags);
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.viewModel.setTags([]);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
  }

  createTag(tag) {
    this.baseComponent.showProgressHub();

    return this.tagClient
      .createTag(tag)
      .then(() => {
        this.viewModel.notifyTagCreated();
        this.baseComponent.showMessageSuccess();
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.baseComponent.showMessageError();
      })
      .then(() => {
        this.baseComponent.dismissProgressHub();
      });
  }

  deleteTag(idTag) {
    this.baseComponent.showProgressHub();

    return this.tagClient
      .deleteTag(idTag)
      .then(() => {
        this.baseComponent.showMessageSuccess();
        this.baseComponent.dismissProgressHub();
      })
      .catch((error) => {
        this.logger.error('Details of the error:', error);
        this.baseComponent.showMessageError();
        this.baseComponent.dismissProgressHub();
        Promise.reject(error);
      });
  }

  setViewModel(value) {
    this.viewModel = value;
  }

  showErrorMessage(value) {
    this.baseComponent.showMessageError(value);
  }
}
