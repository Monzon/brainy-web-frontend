import { bindable } from 'aurelia-framework';

export class BrainyAutocompleteDropDownList {
  @bindable() customSelectValue;
  @bindable() values;

  selectValue(value) {
    this.customSelectValue(value);
  }

}
